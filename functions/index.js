const functions = require("firebase-functions");

const express = require('express');
const cors = require('cors');
let bodyParser = require('body-parser');
const {parse} = require('querystring');
const fs = require('fs');

const headers = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST",
    "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    "Content-Type": "*"
};

const fileUpload = express();

fileUpload.use(bodyParser.json());
// Automatically allow cross-origin requests
fileUpload.use(cors({origin: true}));

fileUpload.use(express.urlencoded({ extended: true }));

// Parse JSON bodies (as sent by API clients)
fileUpload.use(express.json());

let admin = require("firebase-admin");

let serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: `${serviceAccount.project_id}.appspot.com`
});

const bucket = admin.storage().bucket();

fileUpload.post('/', async function (req, res) {
    res.headers = headers
    let inputData = typeof req.body === "object" ? req.body : parse(req.body);
    console.log(serviceAccount.project_id)
    let fileName = req.query.name || 'test';

    let filePath = `files/${fileName}.docx`;

    fs.writeFile(filePath, inputData, async (err) => {
        if(!err) {
            let uploadRes = await bucket.upload(filePath, {
                destination: `${fileName}.docx`,
            }).catch(e => console.log(e));
            fs.unlinkSync(filePath);
            const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${serviceAccount.project_id}.appspot.com/o/${fileName}.docx?alt=media`;
            console.log(imageUrl);
            res.send(imageUrl);
        } else {
            res.send('could not create the file');
        }
    });
});


exports.fileUpload = functions.https.onRequest(fileUpload);

