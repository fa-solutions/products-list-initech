async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).catch(e => console.error(e));
    return response?.json() || null;
}



let recordSample = {
    "batchMetricsTotalFormatted":"$0",
    "batchMetricsTotal":0,
    "timelineFormatted":null,
    "timeline":null,
    "proposalNumberFormatted":"QUO101030",
    "proposalNumber":"QUO101030",
    "createdFormatted":"2021-03-16T18:30:02.349Z",
    "created":"2021-03-16T18:30:02.349Z",
    "engineeringTotalFormatted":"$0",
    "engineeringTotal":0,
    "versionFormatted":null,
    "version":null,
    "updatedFormatted":"2021-07-01T16:20:56.253Z",
    "updated":"2021-07-01T16:20:56.253Z",
    "paymentTypeFormatted":null,
    "paymentType":{
        "id":null,
        "value":null
    },
    "metricsAndEngineeringTotalFormatted": "$102,844",
    "hardwareTotalFormatted":"$57,844",
    "hardwareTotal":57844,
    "primaryProposalFormatted":null,
    "primaryProposal":null,
    "labVShopFloorFormatted":null,
    "labVShopFloor":{
        "id":null,
        "value":null
    },
    "proposalTitleFormatted":"Test Proposal",
    "proposalTitle":"Test Proposal",
    "proposalTotalFormatted":"$57,844",
    "proposalTotal":57844,
    "updatedByFormatted":"Rich DiBernardo",
    "updatedBy":{
        "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
        "value":"Rich DiBernardo"
    },
    "25MilestoneFormatted":null,
    "25Milestone":{
        "id":null,
        "value":null
    },
    "lecronFormatted":null,
    "lecron":null,
    "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
    "lastActivity":"2021-07-01T16:20:55.000Z",
    "proposalOwnerFormatted":"Barbara Brzuchacz",
    "proposalOwner":{
        "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
        "value":"Barbara Brzuchacz"
    },
    "createdByFormatted":"Rich DiBernardo",
    "createdBy":{
        "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
        "value":"Rich DiBernardo"
    },
    "engineeringTemplateFormatted":"Yes",
    "engineeringTemplate":{
        "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
        "value":"Yes"
    },
    "proposalOwnerTitleFormatted":null,
    "proposalOwnerTitle":null,
    "projectTeam":{
        "ids":[

        ],
        "values":[

        ]
    },
    "hardwareTemplateFormatted":"No",
    "hardwareTemplate":{
        "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
        "value":"No"
    },
    "proposalCompanyFormatted":"Test Account",
    "proposalCompany":{
        "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
        "value":"Test Account"
    },
    "customerResponsibilities":{
        "ids":[

        ],
        "values":[

        ]
    },
    "masterPlanTemplateFormatted":"No",
    "masterPlanTemplate":{
        "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
        "value":"No"
    },
    "startDate":null,
    "customerFormatted":null,
    "customer":{
        "id":null,
        "value":null
    },
    "batchMetricsTemplateFormatted":"No",
    "batchMetricsTemplate":{
        "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
        "value":"No"
    },
    "expectedClosedDate":null,
    "titleFormatted":"Head of Operations",
    "title":"Head of Operations",
    "closedDate":null,
    "projectNameFormatted":"Test",
    "projectName":{
        "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
        "value":"Test"
    },
    "expectedDaysToCloseFormatted":"0",
    "expectedDaysToClose":0,
    "cityFormatted":null,
    "city":null,
    "stateFormatted":null,
    "state":null,
    "daysToCloseFormatted":"0",
    "daysToClose":0,
    "deliveredOnTimeFormatted":false,
    "deliveredOnTime":false,
    "zipCodeFormatted":null,
    "zipCode":null,
    "putInRichsSignatureFormatted":"false",
    "putInRichsSignature":"false",
    "customerFirstNameFormatted":"Kenton",
    "customerFirstName":"Kenton",
    "instanceId":"637de31a-2a2e-44d0-ad8b-e217924283bb",
    "batchMetrics":[],
    "hardwareProducts":[
        {
            "idFormatted":"QUO104324",
            "id":"QUO104324",
            "productHardwareFormatted":"Male NPT to 1/2\" TriClamp",
            "productHardware":{
                "id":"36331d94-59fa-47b3-b83c-acef6a7d6df5",
                "value":"Male NPT to 1/2\" TriClamp"
            },
            "descriptionHardwareFormatted":"",
            "descriptionHardware":"Male NPT to 1/2\" TriClamp Adapter",
            "listPriceHardwareFormatted":"$41",
            "listPriceHardware":"41",
            "ourPriceHardwareFormatted":"$27",
            "ourPriceHardware":"27",
            "quantityHardwareFormatted":"10",
            "quantityHardware":10,
            "lineTotalHardwareFormatted":"$410",
            "lineTotalHardware":410,
            "activeHardwareFormatted":"Yes",
            "activeHardware":"Yes",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "timelineFormatted":null,
            "timeline":null,
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "lecronFormatted":null,
            "lecron":null,
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "cityFormatted":null,
            "city":null,
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "titleFormatted":"Head of Operations",
            "title":"Head of Operations",
            "customerFirstNameFormatted":"Kenton",
            "customerFirstName":"Kenton",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        },
        {
            "idFormatted":"QUO104314",
            "id":"QUO104314",
            "productHardwareFormatted":"Lecron 17\" SS Touch Screen Monitor",
            "productHardware":{
                "id":"4d99dab3-c502-4cdb-a8c8-434b2590fca6",
                "value":"Lecron 17\" SS Touch Screen Monitor"
            },
            "descriptionHardwareFormatted":"",
            "descriptionHardware":"",
            "listPriceHardwareFormatted":"$1,027",
            "listPriceHardware":"1027",
            "ourPriceHardwareFormatted":"$875",
            "ourPriceHardware":"875",
            "quantityHardwareFormatted":"12",
            "quantityHardware":12,
            "lineTotalHardwareFormatted":"$12,324",
            "lineTotalHardware":12324,
            "activeHardwareFormatted":"Yes",
            "activeHardware":"Yes",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "timelineFormatted":null,
            "timeline":null,
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "lecronFormatted":null,
            "lecron":null,
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "cityFormatted":null,
            "city":null,
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "titleFormatted":"Head of Operations",
            "title":"Head of Operations",
            "customerFirstNameFormatted":"Kenton",
            "customerFirstName":"Kenton",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        },
        {
            "idFormatted":"QUO104313",
            "id":"QUO104313",
            "productHardwareFormatted":"Lecron PC",
            "productHardware":{
                "id":"19076751-ced1-4df9-8a97-19c2517dc02b",
                "value":"Lecron PC"
            },
            "descriptionHardwareFormatted":"",
            "descriptionHardware":"Tank Farm PC Replacement - Installation - Setup and Configuration",
            "listPriceHardwareFormatted":"$1,450",
            "listPriceHardware":"1450",
            "ourPriceHardwareFormatted":"$1,232",
            "ourPriceHardware":"1232",
            "quantityHardwareFormatted":"31",
            "quantityHardware":31,
            "lineTotalHardwareFormatted":"$44,950",
            "lineTotalHardware":44950,
            "activeHardwareFormatted":"Yes",
            "activeHardware":"Yes",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "timelineFormatted":null,
            "timeline":null,
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "lecronFormatted":null,
            "lecron":null,
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "cityFormatted":null,
            "city":null,
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "titleFormatted":"Head of Operations",
            "title":"Head of Operations",
            "customerFirstNameFormatted":"Kenton",
            "customerFirstName":"Kenton",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        },
        {
            "idFormatted":"QUO104312",
            "id":"QUO104312",
            "productHardwareFormatted":"1/2\" NPT Valve",
            "productHardware":{
                "id":"d857d482-5d44-4ab3-866d-43f454861cbe",
                "value":"1/2\" NPT Valve"
            },
            "descriptionHardwareFormatted":"",
            "descriptionHardware":"For Lay Down Rack",
            "listPriceHardwareFormatted":"$80",
            "listPriceHardware":"80",
            "ourPriceHardwareFormatted":"$52.80",
            "ourPriceHardware":"52.8",
            "quantityHardwareFormatted":"2",
            "quantityHardware":2,
            "lineTotalHardwareFormatted":"$160",
            "lineTotalHardware":160,
            "activeHardwareFormatted":"Yes",
            "activeHardware":"Yes",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "timelineFormatted":null,
            "timeline":null,
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "lecronFormatted":null,
            "lecron":null,
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "cityFormatted":null,
            "city":null,
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "titleFormatted":"Head of Operations",
            "title":"Head of Operations",
            "customerFirstNameFormatted":"Kenton",
            "customerFirstName":"Kenton",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        }
    ],
    "engineering":[
        {
            "idFormatted":"ENG111980",
            "id":"ENG111980",
            "productEngineeringFormatted":"Time Schedule management / update",
            "productEngineering":{
                "id":"8d892a1b-9ffd-4633-8092-0e3f6dd36e00",
                "value":"Time Schedule management / update"
            },
            "descriptionEngineeringFormatted":"Time Schedule management / update",
            "descriptionEngineering":"Time Schedule management / update",
            "position":{
                "id":null,
                "value":null
            },
            "perHourPriceFormatted":"$20",
            "perHourPrice":20,
            "estimatedHoursEngineeringFormatted":null,
            "estimatedHoursEngineering":null,
            "lineTotalEngineeringFormatted":"$0",
            "lineTotalEngineering":0,
            "activeEngineeringFormatted":"No",
            "activeEngineering":"No",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "timelineFormatted":null,
            "timeline":null,
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lecronFormatted":null,
            "lecron":null,
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "cityFormatted":null,
            "city":null,
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        },
        {
            "idFormatted":"ENG111958",
            "id":"ENG111958",
            "productEngineeringFormatted":"Utility flow diagrams",
            "productEngineering":{
                "id":"72a3a045-06d6-49ce-b750-1d18aca4ba39",
                "value":"Utility flow diagrams"
            },
            "descriptionEngineeringFormatted":"Utility flow diagrams",
            "descriptionEngineering":"Utility flow diagrams",
            "position":{
                "id":null,
                "value":null
            },
            "perHourPriceFormatted":"$1",
            "perHourPrice":1,
            "estimatedHoursEngineeringFormatted":null,
            "estimatedHoursEngineering":null,
            "lineTotalEngineeringFormatted":"$0",
            "lineTotalEngineering":0,
            "activeEngineeringFormatted":"No",
            "activeEngineering":"No",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "timelineFormatted":null,
            "timeline":null,
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lecronFormatted":null,
            "lecron":null,
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "cityFormatted":null,
            "city":null,
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        },
        {
            "idFormatted":"ENG111957",
            "id":"ENG111957",
            "productEngineeringFormatted":"Progress Meetings",
            "productEngineering":{
                "id":"2916df14-b7d0-4319-9dbb-af7cf9eedc54",
                "value":"Progress Meetings"
            },
            "descriptionEngineeringFormatted":"Progress Meetings (face-to-face, by phone or video)",
            "descriptionEngineering":"Progress Meetings (face-to-face, by phone or video)",
            "position":{
                "id":null,
                "value":null
            },
            "perHourPriceFormatted":"$1",
            "perHourPrice":1,
            "estimatedHoursEngineeringFormatted":null,
            "estimatedHoursEngineering":null,
            "lineTotalEngineeringFormatted":"$0",
            "lineTotalEngineering":0,
            "activeEngineeringFormatted":"No",
            "activeEngineering":"No",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "timelineFormatted":null,
            "timeline":null,
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lecronFormatted":null,
            "lecron":null,
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "cityFormatted":null,
            "city":null,
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        },
        {
            "idFormatted":"ENG111956",
            "id":"ENG111956",
            "productEngineeringFormatted":"Equipment list (incl. material and instrumentation)",
            "productEngineering":{
                "id":"bbd6dfe2-0f83-4b4b-baed-ba770d3626af",
                "value":"Equipment list (incl. material and instrumentation)"
            },
            "descriptionEngineeringFormatted":"Equipment list (incl. material and instrumentation)",
            "descriptionEngineering":"Equipment list (incl. material and instrumentation)",
            "position":{
                "id":null,
                "value":null
            },
            "perHourPriceFormatted":"$2",
            "perHourPrice":2,
            "estimatedHoursEngineeringFormatted":null,
            "estimatedHoursEngineering":null,
            "lineTotalEngineeringFormatted":"$0",
            "lineTotalEngineering":0,
            "activeEngineeringFormatted":"No",
            "activeEngineering":"No",
            "parentIdFormatted":"QUO101030",
            "parentId":{
                "id":"637de31a-2a2e-44d0-ad8b-e217924283bb",
                "value":"QUO101030"
            },
            "timelineFormatted":null,
            "timeline":null,
            "createdFormatted":"2021-03-16T18:30:02.349Z",
            "created":"2021-03-16T18:30:02.349Z",
            "updatedFormatted":"2021-07-01T16:20:56.253Z",
            "updated":"2021-07-01T16:20:56.253Z",
            "proposalTitleFormatted":"Test Proposal",
            "proposalTitle":"Test Proposal",
            "paymentTypeFormatted":null,
            "paymentType":{
                "id":null,
                "value":null
            },
            "proposalOwnerFormatted":"Barbara Brzuchacz",
            "proposalOwner":{
                "id":"51f79de4-c464-483a-a57e-5c77b7c9be91",
                "value":"Barbara Brzuchacz"
            },
            "labVShopFloorFormatted":null,
            "labVShopFloor":{
                "id":null,
                "value":null
            },
            "proposalOwnerTitleFormatted":null,
            "proposalOwnerTitle":null,
            "updatedByFormatted":"Rich DiBernardo",
            "updatedBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "25MilestoneFormatted":null,
            "25Milestone":{
                "id":null,
                "value":null
            },
            "lecronFormatted":null,
            "lecron":null,
            "lastActivityFormatted":"2021-07-01T16:20:55.000Z",
            "lastActivity":"2021-07-01T16:20:55.000Z",
            "createdByFormatted":"Rich DiBernardo",
            "createdBy":{
                "id":"7d3daf09-f461-4486-bafc-e1e55fb5147b",
                "value":"Rich DiBernardo"
            },
            "proposalCompanyFormatted":"Test Account",
            "proposalCompany":{
                "id":"664fd9a4-e328-43c6-92d0-253c2d7e6187",
                "value":"Test Account"
            },
            "projectTeamFormatted":null,
            "projectTeam":{
                "ids":[

                ],
                "values":[

                ]
            },
            "customerFormatted":null,
            "customer":{
                "id":null,
                "value":null
            },
            "customerResponsibilitiesFormatted":null,
            "customerResponsibilities":{
                "ids":[

                ],
                "values":[

                ]
            },
            "projectNameFormatted":"Test",
            "projectName":{
                "id":"ff2035de-6ed2-45cf-b265-0217d8768393",
                "value":"Test"
            },
            "cityFormatted":null,
            "city":null,
            "stateFormatted":null,
            "state":null,
            "zipCodeFormatted":null,
            "zipCode":null,
            "putInRichsSignatureFormatted":"false",
            "putInRichsSignature":"false",
            "batchMetricsTotalFormatted":"$0",
            "batchMetricsTotal":"0",
            "engineeringTotalFormatted":"$0",
            "engineeringTotal":"0",
            "hardwareTotalFormatted":"$57,844",
            "hardwareTotal":"57844",
            "proposalTotalFormatted":"$57,844",
            "proposalTotal":"57844",
            "engineeringTemplateFormatted":"Yes",
            "engineeringTemplate":{
                "id":"837848b5-a4ce-463c-bd1c-c3d01c2496f2",
                "value":"Yes"
            },
            "hardwareTemplateFormatted":"No",
            "hardwareTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "masterPlanTemplateFormatted":"No",
            "masterPlanTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "batchMetricsTemplateFormatted":"No",
            "batchMetricsTemplate":{
                "id":"f814fb6a-3b94-47fb-ae6b-429ceed4f2c5",
                "value":"No"
            },
            "primaryProposalFormatted":null,
            "primaryProposal":null,
            "versionFormatted":null,
            "version":null,
            "startDate":null,
            "expectedClosedDate":null,
            "closedDate":null,
            "expectedDaysToCloseFormatted":"0",
            "expectedDaysToClose":"0",
            "deliveredOnTimeFormatted":"false",
            "deliveredOnTime":"false",
            "daysToCloseFormatted":"0",
            "daysToClose":"0"
        }
    ]
}