let FAClient = null;
let recordId = null;
let linesGlobal = {
    "hardware" : null,
    "engineering" : null,
    "modules": null
};
let productList = null;

let lineIds = {
    "hardware" : "6b984f98-ba1c-4c7d-9d89-0e64495b5473",
    "engineering": "1b39a3b3-23a7-4147-b225-7ae1a61e6073",
    "modules": "75d55e56-0b8f-4839-a8a8-289c61302949"
}

const delay = t => new Promise(resolve => setTimeout(resolve, t));

// aHR0cDovL2xvY2FsaG9zdDo1MDAw localhost:5000
// aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3Byb2R1Y3RzLWxpc3QtaW5pdGVjaA== // https://fa-solutions.gitlab.io/products-list-initech

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3Byb2R1Y3RzLWxpc3QtaW5pdGVjaA==`,
};

let config = {
     lines: {
         name: '',
         fields: {}
     },
    hardware: {
        name: '',
        fields: {}
    },
    engineering: {
        name: '',
        fields: {}
    },
    module: {
        name: '',
        fields: {}
    },
     products: {
         name: 'product',
         fields: {}
     },

     parent : {
         name: ''
     },
     helpText: {
         key: '',
         value: null
     },
     style: {
         input: {
             width : null
         },
         text: {
             mainTextSize: null,
             helpTextSize: null
         }
     }
}


function startupService() {
    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    const {
        productsApp= 'product',
        parentApp= 'quote',
        linesApp= 'quote_line',
        hardwareApp= 'quote_line',
        engineeringApp= 'engineering',
        modulesApp= 'batch_metric',
    } = FAClient.params;

    const {
        productName = `${productsApp}_field0`,
        productPrice = `${productsApp}_field3`,
        productOurPrice = `${productsApp}_field4`,
        productDescription = `${productsApp}_field8`,
        productPerHourPrice = `${productsApp}_field10`,
        productCategory = `${productsApp}_field9`,
        helpText= 'In Stock',
        helpTextField= null,
        productImg = `${productsApp}_field3`,
        lineName = `${linesApp}_field13`,
        lineQty = `${linesApp}_field17`,
        hardwareLinesName = `${hardwareApp}_field13`,
        hardwareLinesQty = `${hardwareApp}_field17`,
        hardwareLinesOurPrice = `${hardwareApp}_field16`,
        engineeringLinesName = `${engineeringApp}_field0`,
        engineeringLinesQty = `${engineeringApp}_field4`,
        moduleLinesName = `${modulesApp}_field0`,
        moduleLinesQty = `${modulesApp}_field4`,
        moduleLinesOurPrice = `${modulesApp}_field3`,
        inputWidth= null,
        mainTextSize=null,
        helpTextSize=null
    } = FAClient.params;

    config.products.name = productsApp;
    config.parent.name = parentApp;
    config.lines.name = linesApp;
    config.hardware.name = hardwareApp;
    config.engineering.name = engineeringApp;
    config.module.name = modulesApp;

    config.helpText.key = helpText;
    config.helpText.valueField = helpTextField;

    config.style.input.width = inputWidth;

    config.style.text = {
        mainTextSize,
        helpTextSize
    }

    config.products.fields = {
        name: productName,
        price: productPrice,
        ourPrice: productOurPrice,
        img: productImg,
        description: productDescription,
        category: productCategory
    };

    config.lines.fields = {
        name: lineName,
        qty: lineQty
    };
    config.hardware.fields = {
        name: hardwareLinesName,
        qty: hardwareLinesQty,
        ourPrice: hardwareLinesOurPrice
    };
    config.engineering.fields = {
        name: engineeringLinesName,
        qty: engineeringLinesQty
    };
    config.module.fields = {
        name: moduleLinesName,
        qty: moduleLinesQty,
        ourPrice: moduleLinesOurPrice
    };


    FAClient.on("openProducts", ({ record }) => {
        console.log(record);
        recordId = record.id;

        let selectValue = document.getElementById('search-select')?.value || 'all';
       console.log(config.hardware.name, config.engineering.name, config.module.name)
        let hardwareLines =  getIncludedLines(config.hardware.name, recordId);
        let engineeringLines =  getIncludedLines(config.engineering.name, recordId);
        let moduleLines =  getIncludedLines(config.module.name, recordId);

        Promise.all([hardwareLines, engineeringLines, moduleLines]).then((values) => {
            console.log(values)
            let [hardwareRes , engineeringRes, moduleRes] = values;
            linesGlobal["hardware"] = hardwareRes;
            linesGlobal["engineering"] = engineeringRes;
            linesGlobal["modules"] = moduleRes;
            console.log({hardwareRes, engineeringRes, moduleRes});
            FAClient.listEntityValues({entity: config.products.name, limit: 1000, offset: 0}, async (data) => {
                productList = data;
                console.log(productList)
                FAClient.open();
                let filteredList = productList;
                if(selectValue !== 'all' && selectValue !== 'included') {
                    filteredList = filterProducts(productList,"category", lineIds[selectValue]);
                    console.log({filteredList, moduleId: lineIds[selectValue]})
                }
                if(selectValue === 'included') {
                    filteredList = filterProducts(productList,"category", "included");
                }
                generateList({ products: filteredList })
            })
        });
    });

    let searchEl = document.getElementById('search-input');
    let searchSelectEl = document.getElementById('search-select');

    searchEl.addEventListener("keyup", (event) => {
        let searchValue = event.target.value;
        let lineKey = document.getElementById('search-select')?.value;
        if (productList) {
            let filteredList = productList;
            if(lineKey !== 'all' && lineKey !== 'included') {
                let filteredByCategory = filterProducts(productList,["category"], lineIds[lineKey]);
                filteredList = filterProducts(filteredByCategory,["name", "description", "category"], searchValue);
            }
            if(lineKey === 'included') {
                filteredList = filterProducts(productList,"category", "included");
                filteredList = filterProducts(filteredList,["name", "description", "category"], searchValue);
            }

            generateList({ products:filteredList })

            console.log(filteredList)
        }
    });

    searchSelectEl.addEventListener("change", (event) => {
        let selectValue = event.target.value;
        if (productList) {
            let filteredList = filterProducts(productList,["category"], selectValue);
                generateList({ products:filteredList })
        }
    });

}

function generateList({products}) {
    let list = document.getElementById('order-list');
    list.innerHTML = '';
    let lineKey = '';
    products.map(product => {
        let linesGlobalKeys = Object.keys(linesGlobal);
        let lineMatch = null;
        linesGlobalKeys.map(key => {
            return linesGlobal[key].find(line => {
                if(line?.field_values[config[key]?.fields.name]?.value === product.id) {
                    lineKey = key;
                    lineMatch = line;
                    return true;
                }
                return false;
            });
        });
        let qty = 0;
        if (lineMatch) {
           qty = lineMatch.field_values[config[lineKey].fields.qty].display_value;
        }
        let fieldValues = product.field_values;
        let priceField = fieldValues[config.products.fields.price];
        let ourPriceValue = fieldValues[config.products.fields.ourPrice]?.formatted_value || 0;
        let priceFormated = priceField?.formatted_value || '';
        let productName = fieldValues[config.products.fields.name].display_value || '';
        let productDescription = fieldValues[config.products.fields.description]?.display_value || '';
        let liEl = document.createElement('li');
        if(productDescription !== '') {
            let description = document.createElement('div');
            description.innerHTML = productDescription;
            productDescription = description.textContent;
        }

        let productCategory = fieldValues[config.products.fields.category].display_value || '';

        liEl.innerHTML = `
            <div class="product-info-container">
              <div class="info-field-container">
                <span class="field-label">Name:</span>
                <span class="product-info-text">${productName}</span>
              </div>
              <div class="info-field-container">
                <span class="field-label">Description:</span>
                <span class="product-info-text">${productDescription}</span>
              </div>
              <div class="info-field-container">
                <span class="field-label">Category:</span>
                <span class="product-info-text">${productCategory}</span>
              </div>
            </div>
            <div class="qty-container">
              <div class="info-field-container">
                    <span class="field-label">List: </span>
                     <div class="input-field-container">
                        <span>${priceFormated}</span>
                     </div>
              </div>
              <div class="info-field-container">
                    <span class="field-label">Price: </span>
                     <div class="input-field-container">
                        <span>${ourPriceValue}</span>
                     </div>
              </div>
               <div class="info-field-container">
                <span class="field-label">${productCategory.includes('Engineering') ? 'Hours:': 'Qty:'} </span>
                <div class="input-field-container">
                    <input 
                       min="0"
                       type="number" 
                       value=${qty}
                       class="qty-input" />
               </div>
              </div>
            </div>
        `;


        let qtyInput = liEl.querySelector('.qty-input');

        qtyInput.addEventListener("change", (event) => {
            let qty = event.target.valueAsNumber;
            let linesGlobalKeys = Object.keys(linesGlobal);
            let lineMatch = null;
            let linesKey = null;
            linesGlobalKeys.map(key => {
                return linesGlobal[key].find(line => {
                    if(line?.field_values[config[key]?.fields.name]?.value === product.id) {
                        linesKey = key;
                        lineMatch = line;
                        return true;
                    }
                    return false;
                });
            });
            //let lineFound = linesGlobal[linesKey].find(line => line.field_values[config[linesKey].fields.name].value === product.id);
            if (lineMatch) {
                if (qty > 0) {
                    console.log('it is running')
                    updateQty({id: lineMatch.id, qty, linesKey});
                    updateRecord();
                } else {
                    deleteEntity(lineMatch.id, linesKey)
                    linesGlobal[linesKey] = linesGlobal[linesKey].filter(line => line.field_values[config[linesKey].fields.name].value !== product.id);
                    lineMatch = null;
                    updateRecord();
                }
            } else {
                if (qty > 0) {
                    addItem({ product, qty, linesKey: product?.field_values[config.products.fields.category].display_value?.toLowerCase()});
                }
            }
        })
        list.appendChild(liEl);
    });
}

function deleteEntity(id, linesKey = "hardware") {
    let updatePayload = {
        entity: config[linesKey].name,
        id: id,
        field_values: { deleted: true }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
        updateRecord();
    });
}


function updateQty({ id, qty, linesKey = "hardware" }) {
    let updatePayload = {
        entity: config[linesKey].name,
        id: id,
        field_values: {
            [config[linesKey].fields.qty]: qty,
        }
    }

    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
        updateRecord();
    })
    console.log(id, qty);
}

function addItem({product, qty,  linesKey = "hardware" }) {
    console.log({linesKey, res: config[linesKey]})
    let payload = {
        entity: config[linesKey].name,
        field_values: {
            [config[linesKey].fields.name] : product.id,
            [config[linesKey].fields.qty]: qty,
            parent_entity_reference_id: ""
        }
    };
    payload.field_values.parent_entity_reference_id = recordId;
    console.log(payload);
    FAClient.createEntity(payload, (data) => {
        console.log(data?.entity_value);
        updateRecord();
        FAClient.navigateTo(`/entity/quote/view/d8598ee3-f2bb-4274-9209-62dbe47d2762`);
        FAClient.navigateTo(`/quote/view/${recordId}`);
        if(!linesGlobal[linesKey]) {
            linesGlobal[linesKey] = [data?.entity_value]
        } else {
            linesGlobal[linesKey].push(data?.entity_value);
        }

    })
}

function updateRecord(id=recordId) {
    let updatePayload = {
        entity: config.parent.name,
        id: id,
        field_values: {}
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}


function filterProducts(products, filters, qualifier = '') {
    if(!filters || qualifier === 'all') {
        return products;
    }

    if(typeof filters === "string") {
        filters = [filters]
    }

    if(qualifier === 'included') {
        let linesObject = {};
        let linesGlobalKeys = Object.keys(linesGlobal);
        linesGlobalKeys.map(key => {
             linesGlobal[key].map(line => {
                 linesObject[line?.field_values[config[key]?.fields.name]?.value] = line;
            });
        });
        console.log({linesObject})
        return products.filter(prod => {
            return linesObject[prod.id];
        })
    }

    if(lineIds[qualifier.toLowerCase()]) {
        qualifier = lineIds[qualifier.toLowerCase()];
    }
    console.log({qualifier})
    return products.filter(prod => {
        return filters.some(f => prod.field_values[config.products.fields[f]]?.value?.toLowerCase().includes(qualifier.toLowerCase()))
    })
}



async function getIncludedLines(lineName, parentId) {
    console.log({lineName})
    let variables = {
        entity: lineName,
        filters: [
            {
                field_name: "parent_entity_reference_id",
                operator: "includes",
                values: [parentId],
            },
        ],
    };
    let linesIncluded = null;

    FAClient.listEntityValues(variables, (lines) => {
        console.log({[lineName]: lines})
        linesIncluded = lines;
    });

    return new Promise(resolve => delay(500).then(() => {
            for(let i = 0; i < 5; i++) {
                if(linesIncluded) {
                    resolve(linesIncluded)
                } else {
                    delay(500).then(() => {
                        if(linesIncluded) {
                            resolve(linesIncluded || [])
                        }
                    })
                }
            }
    }))
}
